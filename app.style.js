import { StyleSheet, StatusBar } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "lightgrey",
  },
  rowContainer: {
    flexDirection: "row",
  },
  categoryContainer: {
    flex: 1,
  },
  viewDate: {
    backgroundColor: "white",
    borderWidth: 4,
    borderColor: "grey",
  },
  textDate: {
    fontSize: 30,
    fontWeight: "bold",
    color: "blue",
  },
});

export default styles;
