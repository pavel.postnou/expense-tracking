// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCOdryLb3bUrCmzFsIbagTAfx8FQt-9JKw",
  authDomain: "expense-tracking-5c83f.firebaseapp.com",
  projectId: "expense-tracking-5c83f",
  storageBucket: "expense-tracking-5c83f.appspot.com",
  messagingSenderId: "147469697671",
  appId: "1:147469697671:web:5ab31fcdba800bfe08ae22",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app)