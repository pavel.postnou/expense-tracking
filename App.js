import React, { useEffect, useState } from "react";
import { View, Text, Button } from "react-native";
import CategoryItem from "./components/categoryItem";
import DateTimePicker from "@react-native-community/datetimepicker";
import styles from "./app.style";

export default function App() {
  const date = new Date();
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [dbDate, setDbDate] = useState(
    new Date(Date.now()).toISOString().slice(0, 7)
  );

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDatePicker(false);
    setDbDate(currentDate.toISOString().slice(0, 7));
  };

  const showMode = () => {
    setShowDatePicker(true);
  };

  return (
    <View style={styles.container}>
      <View style={styles.viewDate}>
        <Text style={styles.textDate}>
          {monthNames[parseInt(dbDate.slice(5, 7)) - 1] +
            " " +
            dbDate.slice(0, 4)}
        </Text>
      </View>
      <View style={styles.rowContainer}>
        <CategoryItem
          color="#f59b42"
          title="Food"
          info={["your expense this month"]}
          icon="local-dining"
          docId="FoodDoc"
          dbDate={dbDate}
          type="food"
        />
        <CategoryItem
          color="#5d76f5"
          title="Car"
          info={["your expense this month"]}
          icon="directions-car"
          docId="CarDoc"
          dbDate={dbDate}
          type="car"
        />
        <CategoryItem
          color="red"
          title="Health"
          info={["your expense this month"]}
          icon="directions-car"
          docId="HealthDoc"
          dbDate={dbDate}
          type="health"
        />
      </View>
      <View style={styles.rowContainer}>
        <CategoryItem
          color="#b05a21"
          title="Home"
          info={["your expense this month"]}
          icon="apartment"
          docId="HomeDoc"
          dbDate={dbDate}
          type="home"
        />
        <CategoryItem
          color="#44c272"
          title="Clothes"
          info={["your expense this month"]}
          icon="checkroom"
          docId="ClothesDoc"
          dbDate={dbDate}
          type="clothes"
        />
        <CategoryItem
          color="#c4c354"
          title="Gifts"
          info={["your expense this month"]}
          icon="card-giftcard"
          docId="GiftsDoc"
          dbDate={dbDate}
          type="gifts"
        />
      </View>
      <View>
        <View>
          <Button onPress={showMode} title="Choose month" />
        </View>
        {showDatePicker && (
          <DateTimePicker
            testID="dateTimePicker"
            maximumDate={new Date(Date.now())}
            value={date}
            mode="date"
            is24Hour={true}
            display="default"
            onChange={onChange}
          />
        )}
      </View>
    </View>
  );
}
