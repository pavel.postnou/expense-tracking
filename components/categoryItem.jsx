import React, { useEffect, useState } from "react";
import { View, Modal, Text, Pressable, TextInput } from "react-native";
import { PricingCard } from "react-native-elements";
import {
  getDoc,
  doc,
  updateDoc,
  setDoc,
  onSnapshot,
  increment,
} from "firebase/firestore";
import { db } from "../firebase/firebase-config";
import styles from "./categoryItem.style";
import { useCallback } from "react";

const CategoryItem = ({ ...props }) => {
  const { color, title, info, icon, docId, dbDate, type } = props;
  const [sum, setSum] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [value, setValue] = useState(null);

  useEffect(() => {
    const handleSum = async () => {
      const docRef = doc(db, dbDate, docId);
      const docSnap = await getDoc(docRef);
      if (!docSnap.data()) {
        await create();
        const docSnap = await getDoc(docRef);
        setSum(docSnap.data().sum);
      } else {
        setSum(docSnap.data().sum);
      }
    };
    handleSum();
  }, [dbDate]);

  const create = useCallback(async () => {
    await setDoc(doc(db, dbDate, docId), {
      type: type,
      sum: 0,
    });
  }, []);

  const change = useCallback(async (value, docId) => {
    await updateDoc(doc(db, dbDate, docId), {
      sum: increment(value),
    });
    setSum(sum + parseInt(value));
  }, []);

  const checkingType = (value) => (!isNaN(value) ? setValue(value) : null);

  const unsub = onSnapshot(doc(db, dbDate, docId), (doc) => {
    doc.data() ? setSum(doc.data().sum) : null;
  });

  return (
    <View style={styles.categoryContainer}>
      <PricingCard
        color={color}
        title={title}
        titleStyle={{ fontSize: 20 }}
        price={`$${sum}`}
        pricingStyle={{ fontSize: 20 }}
        info={info}
        button={{ title: " change", icon: icon }}
        onButtonPress={() => setModalVisible(true)}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              Write the sum of your spendings
            </Text>
            <TextInput
              style={styles.textInputStyle}
              placeholder="amount"
              value={value}
              onChangeText={checkingType}
            />
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => (
                setModalVisible(false), change(value, docId), setValue(null)
              )}
            >
              <Text style={styles.textStyle}>Enter</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default React.memo(CategoryItem);
