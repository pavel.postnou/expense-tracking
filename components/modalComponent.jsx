import React, { useEffect, useState, useCallback } from "react";
import { View, Modal, Text, Pressable, TextInput } from "react-native";
import styles from "./categoryItem.style";

const ModalComponent = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [value, setValue] = useState();
  
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Write the sum of your spendings</Text>
          <TextInput
            style={{
              borderWidth: 1,
              width: 100,
              textAlign: "center",
              marginBottom: 10,
            }}
            placeholder="amount"
            value={value}
            onChangeText={checkingType}
          />
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => (
              setModalVisible(false), change(value, docId), setValue(0)
            )}
          >
            <Text style={styles.textStyle}>Enter</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

export default ModalComponent